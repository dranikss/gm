$(function() {
    /* переключатель этажей */
    $('body').on('click', '.a-floor', function(e){
        e.preventDefault();
        var floor = $(this).attr('href');
        $('.a-floor').removeClass('a-floor--actvie');
        $(this).addClass('a-floor--actvie');

        $('.b-floor').removeClass('b-floor--active');
        $(floor).addClass('b-floor--active');
    });

    /* работа с пинами */
    $('body').on('click', '.b-section', function(e){
        $(this).closest('.b-floor').find('.b-pin').addClass('b-pin--active');
        $('.b-section').removeClass('b-section-disabled');
        $('.b-section').removeClass('b-section-enabled');
        $('.b-section').addClass('b-section-disabled');
        $(this).addClass('b-section-enabled');

        // какое-то действие по подгрузке контента

    });
    $(document).mouseup(function (e){ // событие клика по веб-документу
        var div = $(".b-pin--active"); // тут указываем ID элемента
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
            div.removeClass('b-pin--active'); // скрываем его
            $('.b-section').removeClass('b-section-disabled');
            $('.b-section').removeClass('b-section-enabled');
        }
    });

    /* для мобильного меню */
    $('body').on('click', '.link-mobile-menu', function(e){
        e.preventDefault();
        $('.b-mobile-box').addClass('b-mobile-box--active')
    });
    $('body').on('click', '.close-mobile-menu', function(e){
        e.preventDefault();
        $('.b-mobile-box').removeClass('b-mobile-box--active')
    });
})